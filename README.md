# Vue.js Sample Backend
This is a quick backend I wrote using Ruby + Sinatra when I was giving [Vue.js](http://vuejs.org/) a shot.

Basically, it offers up a few api endpoints for the Vue.js app to access. The actions are

* Get all
* Get one
* Create one
* Destroy one

That's about it!