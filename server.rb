require 'bundler'
Bundler.require

# == Initial Config =======================================

# Set the server
set :server, %w[thin webrick]

# Enable sessions
enable :sessions

before do
  if request.body.size > 0
    request.body.rewind
    @params = JSON.parse(request.body.read)
  end
end

# Allow the proper requests
before '/api/*' do
    content_type :json    
    headers['Access-Control-Allow-Origin'] = 'http://localhost:9000'
    headers['Access-Control-Allow-Methods'] = 'POST, PUT, DELETE, GET, OPTIONS'
    headers['Access-Control-Request-Method'] = '*'
    headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
end

# == DB Config ============================================

# Configure the db connections
# -- dev
configure :development do
	DataMapper.setup(:default, "sqlite3://#{Dir.pwd}/dev.db")
end

# == DB Models ============================================

class Person
	include DataMapper::Resource
	property :id, 					Serial, :key => true
	property :name, 				String
	property :age, 					Integer
end

DataMapper.finalize
DataMapper.auto_migrate!

# == Routes ============================================

# This handles the OPTIONS requests
#   and makes sure they all go through
options '/api/*' do
	200
end

# Get all the records
get '/api/person' do
	data = Person.all()
	return data.to_json
end

# Get one record
get '/api/person/:id' do
	data = Person.get(params['id'])
	return data.to_json

# Destroy a record
delete '/api/person' do
	Person.get(params['id']).destroy
	message = "Record deleted"
	return message.to_json
end

# Create a record
post '/api/person' do
	p = {
		name: params['name'],
		age: params['age']
	}

	person = Person.new(p)

	if person.valid?
		if person.save
			message = person.to_json
		end
	else
		message = person.errors.to_json
	end

	return message
end
